﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 恒温测试机.Model
{
    public class Model_1016
    {
        #region 压力变化测试
        
        public double Pc_Before_1 { get; set; }
        public double Tc_Before_1 { get; set; }
        public double Ph_Before_1 { get; set; }
        public double Th_Before_1 { get; set; }
        public double Qm_Before_1 { get; set; }
        public double Tm_Before_1 { get; set; }

        public double Pm_A_1 { get; set; }
        public double Pm_A_1_diff { get; set; }
        public double Pm_A_1_time { get; set; }

        public double Pm_A_2 { get; set; }
        public double Pm_A_2_diff { get; set; }

        public double Pm_A_3 { get; set; }
        public double Pm_A_3_diff { get; set; }
        public double Pm_A_3_time { get; set; }

        public double Pm_A_4 { get; set; }
        public double Pm_A_4_diff { get; set; }

        public double Pc_Before_2 { get; set; }
        public double Tc_Before_2 { get; set; }
        public double Ph_Before_2 { get; set; }
        public double Th_Before_2 { get; set; }
        public double Qm_Before_2 { get; set; }
        public double Tm_Before_2 { get; set; }

        public double Pm_B_1 { get; set; }
        public double Pm_B_1_diff { get; set; }
        public double Pm_B_1_time { get; set; }

        public double Pm_B_2 { get; set; }
        public double Pm_B_2_diff { get; set; }

        public double Pm_B_3 { get; set; }
        public double Pm_B_3_diff { get; set; }

        public double Pm_B_3_time { get; set; }

        public double Pm_B_4 { get; set; }
        public double Pm_B_4_diff { get; set; }
        public double Pm_Max_diff { get; set; }
        #endregion

        #region 流量减少
        public double Qm_Safety { get; set; }
        public double Qm_Safety2 { get; set; }
        #endregion

        public double Pm_Heat { get; set; }
        public double Pm_Heat_diff { get; set; }        
    }
}
